import Slider from '../../components/Slider/Slider'
import React from 'react'
import './Home.scss'
function Home() {
  return (
    <div className='home'>
      <Slider/>
    </div>
  )
}

export default Home
