import React from 'react'
import './Footer.scss'

function Footer() {
  return (
    <div className='footer'>
      <div className="top">
       <div className="item">
        <h1>Categories</h1>
        <span>Women</span>
        <span>Men</span>
        <span>Shoes</span>
        <span>Accessories</span>
        <span>New Arrivals</span>
       </div>
       <div className="item">
       <h1>Links</h1>
        <span>FAQ</span>
        <span>Pages</span>
        <span>Stores</span>
        <span>Compare</span>
        <span>Cookies</span>
       </div>
       <div className="item">
        <h1>About</h1>
        <spam>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Corrupti error deserunt ducimus sapiente nesciunt assumenda ullam doloribus consequuntur neque minus?</spam>
       </div>
       <div className="item">
       <h1>Contact</h1>
        <spam>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Corrupti error deserunt ducimus sapiente nesciunt assumenda ullam doloribus consequuntur neque minus?</spam>
       </div>
      </div>
      <div className="bottom">

      </div>

    </div>
  )
}

export default Footer
