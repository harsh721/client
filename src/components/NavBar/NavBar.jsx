import React from 'react'
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import SearchIcon from '@mui/icons-material/Search';
import PersonOutlineOutlinedIcon from '@mui/icons-material/PersonOutlineOutlined';
import FavoriteBorderOutlinedIcon from '@mui/icons-material/FavoriteBorderOutlined';
import ShoppingCartCheckoutOutlinedIcon from '@mui/icons-material/ShoppingCartCheckoutOutlined';
import { Link } from 'react-router-dom';
import "./NavBar.scss";

function NavBar() {
  return (
    <div className='navbar'>
        <div className="wrapper">
        <div className='left'>
            <div className="item">
                <img src='' alt=''/>
                <KeyboardArrowDownIcon/>
            </div>
            <div className="item">
                <spam>USD</spam>
                <KeyboardArrowDownIcon/>
            </div>
            <div className="item">
                <Link className="link" to="/products/1">Women</Link>
            </div>
            <div className="item">
                <Link className="link" to="/products/2">Men</Link>
            </div>
            <div className="item">
                <Link className="link" to="/products/3">Children</Link>
            </div>
        </div>
        <div className='center'>
            <Link className="link" to="/">LAMASTORE</Link>
        </div>
        <div className='right'>
        <div className='item'>
            <Link className="link" to="/">Homepage</Link>
        </div>
        <div className='item'>
            <Link className="link" to="/">About</Link>
        </div>
        <div className='item'>
            <Link className="link" to="/">Contact</Link>
        </div>
        <div className='item'>
            <Link className="link" to="/">Stores</Link>
        </div>
        <div className="icons">
            <SearchIcon/>
            <PersonOutlineOutlinedIcon/>
            <FavoriteBorderOutlinedIcon/>
            <div className="cartIcons">
                <ShoppingCartCheckoutOutlinedIcon/>
                <span>0</span>
            </div>
        </div>
        </div>
    </div>
       
    </div>
  )
}

export default NavBar
