import React, { useState } from 'react'
import EastOutlinedIcon from '@mui/icons-material/EastOutlined';
import WestOutlinedIcon from '@mui/icons-material/WestOutlined';
import img1 from '../../img/tshirtpic.jpg';
import img2 from '../../img/tshirt-2.jpg';
import img3 from '../../img/shirtImage.jpg';
import img4 from '../../img/shirtImage.jpg';
import './Slider.scss'

function Slider() {

  const [currentSlide, setCurrentSlide] = useState(0);

  const prevSlide = ()=>{
    setCurrentSlide(currentSlide === 0 ? 1: (prev)=> prev -1 )
  }
  const nextSlide = ()=>{
    setCurrentSlide(currentSlide === 1 ? 0 : (prev)=> prev +1 )
  }

  return (
    <div className='slider'>
      <div className="container" style={{transform:`translateX(-${currentSlide *100}vw)`}}>
        <img src={img1} alt=''/>
        <img src={img2} alt=''/>
        <img src={img3} alt=''/>
       
       
      
      </div>
      <div className="icons">
        <div className="icon" onClick={prevSlide}>
        <WestOutlinedIcon/>
        </div>
        <div className="icon" onClick={nextSlide}>
        <EastOutlinedIcon />
        </div>
      </div>
    </div>
  )
}

export default Slider
